#+TITLE: STORY BOARD OF PROJECT
#+AUTHOR: Mayank and Debojit
#+DATE: [2017-16-05 WED]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

* Introduction
- This is the story board for the Hash table experiment.

* Scope of Project
1. We want to build a project that would help students learn and understand hash tables visually through explanations,examples,exercises and self tests.
2. For realising this pedagogy would be followed.
3. Aim of this project is to benefit as many students across the globe as possible.

* Motivation
- We want to build a teaching aid that can help students across the globe understand fundamental concepts in a better way.
- We want students to enjoy the process of learning and be able to apply and recall better.
 
* Members

|------------------+-----------------------+------------------------|
| *Names*          | Debojit               | Mayank Modi            |
|------------------+-----------------------+------------------------|
| *Year of Study*  | 1st year Btech IIIT-H | 3rd year Btech IIIT-H  |
|------------------+-----------------------+------------------------|
| *Phone-No*       | 9871089660            | 9052447804             |
|------------------+-----------------------+------------------------|
| *Email-ID*       | ddas1500@gmail.com    | fireball4556@gmail.com |
|------------------+-----------------------+------------------------|
| *gitlab handles* | debojit               | MaverickMe4556         |
|------------------+-----------------------+------------------------|
| *github handles* | debo98                | fireballpoint1         |
|                  |                       |                        |
|------------------+-----------------------+------------------------|

* Running Status
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   | *SNo.* | *Purpose*                                               | *Start Date*  | *End Date*    | *Status* | *Remarks* |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   |      1 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/hashtables/milestones/1][Creating Experiment structure for hashtables - A1 phase]] | 14th May,2018 | 21st May,2018 |     100% | Done      |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   |      2 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/hashtables/milestones/2][Creating complete story board for project - A2 phase]]    | 22st May,2018 | 30th May.2018 |     100% | Done      |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   |      3 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/artefacts/milestones/1][Building Artifacts for our 3 learning unit]]              | 30th May,2018 | 09th Jul.2018 |     100% | Done      |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   |      4 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/artefacts/milestones/2][Working with UI team]]                                    | 03rd Jul,2018 | 30th Jun.2018 |      33% | Ongoing   |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   |      5 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/artefacts/milestones/3][Rendering artefacts with catalog service]]                | 1st Jul,2018  | 13th Jul.2018 |     100% | Done      |
   |        |                                                         |               |               |          |           |
   |--------+---------------------------------------------------------+---------------+---------------+----------+-----------|
   
* Assumptions
 
 - Basic understanding of mathematics
 - Understanding of English


* Documents
|--------+----------------------+--------------|
| *SNo.* | *Purpose*            | *Link*       |
|--------+----------------------+--------------|
|      1 | Requirements         | [[./requirements/index.org][req]]          |
|--------+----------------------+--------------|
|      2 | Realization Plan     | [[./realization-plan/index.org][rlz-plan]]     |
|--------+----------------------+--------------|
|      3 | Experiment structure | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/content/blob/master/src/exp-cnt/index.org][exp-cnt]]      |
|--------+----------------------+--------------|
|      4 | Story Board          | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/content/tree/master/src/story-board/index.org][story-board]]  |
|--------+----------------------+--------------|
|      5 | Experience           | [[./experience/index.org][experience]]   |
|--------+----------------------+--------------|
|      6 | UI Artefacts         | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/ui-design/blob/master/src/index.org][UI artefacts]] |
|--------+----------------------+--------------|
* StakeHolders

 - College and School Students
 - MHRD
 - Teachers

* Value Added by our project

 - It would be beneficial for 1st and 2nd year engineering students as Data Structures and Algorithms are usually thought in these years of study.
 - Highly beneficial for tier 2 and tier 3 college students.
 - use this to learn and understand the concept of heap sort.

* Risks and Challenges 
- We faced problems using d3 and SVG in our codes.
- We are faceing issues making our website completely responsive.
- Rendering all our artefacts using the renderer was a challenge. 

* Project Closure
- We have sucesfully rendered our heapsort experiment and hosted it.
- As of now we have not used the UI design provided due to many reasons and issues faced.
- We also want someone to add quiz grammer and render it in our experiment.
- Our goal for the future of this project is to retain the present implementations and style of coding and realisation and implement the UI design given by the UI team.
- We want users to benefit from our experiment.


** User Feedback
- This experiment has not been presented to the final user yet.
- We request the outreach team to carry this forward, evaluate it and make required changes.

** Learnings and Experience
- Got hands on experience with the full cycle of developing a software.
- Understood pedagogy and built a working app in 2 months.
- It was a nice experience overall and we learnt a lot from this internship.
* Hosted URL
  [[http://exp.vlabs.ac.in/experiments/ht/story-board/concrete.html][hashtables]]
* Link to Poster
[[https://gitlab.com/vlead-projects/experiments/posters/blob/master/hashtables/VLabs-A2-poster-FINAL-23.1x16.5Inches.pdf][Poster]]
* Link to Open Issues of the group
[[https://gitlab.com/groups/vlead-projects/experiments/hashtablestwo/-/issues][Open Issues]]
