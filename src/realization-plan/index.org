#+TITLE: Realization Plan
#+AUTHOR: Debojit and Mayank
#+DATE: [2018-05-15 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

* Introduction

This carries the realization plan for heap sort experiment with all milestones specified.

* Milestones
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   | *SNo.* | *Purpose*                                              | *Start Date*  | *End Date*    | *Status* | *Remarks* |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   |      1 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/content/milestones/1][Creating Experiment structure for heap sort - A1 phase]] | 14th May,2018 | 21st May,2018 |     100% | Done      |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   |      2 | [[https://gitlab.com/vlead-projects/experiments/hashtablestwo/content/milestones/2][Creating complete story board for project - A2 phase]]   | 22st May,2018 | 30th May.2018 |     100% | Done      |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   |      3 | Building Artifacts                                     | 30th May,2018 | 25th Jun.2018 |     100% | Done      |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   |      4 | Working with UI                                        | 25th Jun,2018 | 30th Jun.2018 |     100% | Done      |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   |      5 | Rendering artefacts                                    | 1st Jul,2018  | 13th Jul.2018 |     100% | Done      |
   |--------+--------------------------------------------------------+---------------+---------------+----------+-----------|
   
