#+TITLE: Requirements for Heap sort experiment
#+AUTHOR: Debojit and Mayank
#+DATE: [2016-07-1 Sat]
#+PROPERTY: results output
#+PROPERTY: exports code
#+SETUPFILE: ../org-templates/level-1.org
#+options: ^:nil
#+LATEX: Literal LaTeX code for export

* Introduction 
  The requirements of =Hash table experiment= are listed here.

** Requirements
   - Make an interactive lab for heap sort algorithm by rendering templates.
   - Give exercises for building heap and then heapify operation to be solved by the users.
   - Give exercises for heapsort to be solved by the users.
   - Add time complexity demonstration of the heap sort algorithm.
   - Give array as an input and the binary heap should be the output with the functionality of removing the root and then another tree would be formed.
