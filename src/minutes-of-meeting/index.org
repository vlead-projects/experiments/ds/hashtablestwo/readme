#+title: Minutes of Meeting  
#+AUTHOR: VLEAD
#+DATE: [2018-05-14 Mon]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the Minutes of meeting had so far for our project:

|--------+---------------------------------------------------------+------------|
| *SNo.* | *Purpose*                                               | *Link*     |
|--------+---------------------------------------------------------+------------|
|     1. | To Review experiment structure and story board          | [[./2018-05-18-review-mom.org][MOM-1 link]] |
|--------+---------------------------------------------------------+------------|
|     2. | To discuss A2 phase and present exp-cnt and story board | [[./2018-05-25-common-mom.org][MOM-2 link]] |
|--------+---------------------------------------------------------+------------|
|     3. | To present the artefacts we have developed so far.      | [[./2018-06-12-review-mom.org][MOM-3 link]] |
|--------+---------------------------------------------------------+------------|





