#+title: Minutes of Meeting 2018-06-12 
#+AUTHOR: VLEAD
#+DATE: [2018-06-12 Tue]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the minutes of meeting held on 12th June:

** Duration
 From 9:30 to 1

** Attendees
 Prof. Venkatesh Chopella
 Prof. Priya
 Prof Thirumal
 Debojit	
 Mayank Modi
 Team of Vlead interns

** Purpose
 - To present the artefacts we have developed so far.

** Takeaways
 - Got a better understanding on how to improve our present experiment.

** Action
 - Will discuss with UI team and work on improving out design.
 - Discuss with experimental team regarding common artefacts we are using.
