#+title: Minutes of Meeting 2018-05-25 
#+AUTHOR: VLEAD
#+DATE: [2018-05-25 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the minutes of meeting held on 25th may:

** Duration
 From 9:45 to 11:45 

** Attendees
- Proff Venkatesh Chopella
- Proff Priya
- Proff Mrutyunjay 
- All Interns

** Purpose
 - To discuss the experiment content structure developed so far and the entire story board of out project.
 - We had a presentation followed by discussions.
   
** Takeaways
 - We tried to understand subtle differences between A1 and A2 phases.
 - We understood the structure of a webapp and that we would be
   creating all the catalogs involved by linking them to the
   respective ids.
 - A2 is just expanding A1 and adding additional resources.
 - We will do the mapping between pedagogy elements and our tasks and complete
   our scoring function.
 - We can add multiple tags to an artefact.
 - A3 phase is addition of the A2 phase which specifies more in detail
   about navigation and time.A2 mainly consists scene with thier
   contents and interactive elements.
 - We spent lot of time debating whether an artefact like scroll bar
   should be there or not. Finally we decided that those kind of
   details will act better as hint rather than required artefact.
 - Final understanding of experiment of structure:
   A1 : Just say and give an explanation of what you will include.
   A2 : Specifies user interaction
   A3 : Contanis the actual nature of interaction.
 - Our specification of requirements in A2 phase is like suggestions
   to the design team who can decide if it is required or not.
   We could update our tags to 'hint' for this purpose.
 - The quiz,pretest and post test is to be designed differently according to thier purpose.
 - The grammar needs to be checked and agreed about together after discussions.
 
** Action
 - Will add story-board,worklogs,experiences to index of directory.
 - Will start writing notes for documenting experiments.
 - Will link A2 to A1,story-board artefacts should refer to
   concrete.org from exp-cnt.
 - Will add learning objectives and prerequisites to preamble of content
   concrete.org
 - Need to remove interaction/navigation parts like scroll bar from
   story-board.
